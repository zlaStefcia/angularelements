# AngularElements

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 11.0.6.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.To 

## Build

To build the components so that they could be used outside of this project, run `npm run build:elements`. It will generate a `elements.js` file to `elements/`.
This file may later be imported to any html e.g. `<script src="./elements/elements.js"></script>` outside of this project to make custom angular elements available.

## Start the project
It's possible to start this project in two different ways:
- start angular app by running `npm start` and opening `localhost:4200`
- simply opening static `index.html` in the browser (which imports built file `elements.js`) to prove that the components can be used outside of Angular environment
