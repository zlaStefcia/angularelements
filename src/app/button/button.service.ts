import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class ButtonService {
  public _counter = 0;
  constructor() { }

  get counter() {
    return this._counter;
  }

  increaseCounter() {
    this._counter = this._counter + 1;
  }
}
