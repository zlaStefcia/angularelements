import {Component, Input, Output, EventEmitter} from '@angular/core';
import {ButtonService} from './button.service';

enum ButtonType {
  NORMAL = 'normal',
  WARNING = 'warning',
  DANGER = 'danger',
  NO_FILL = 'no-fill'
}
@Component({
  selector: 'vzlb-button',
  templateUrl: './button.component.html',
  styleUrls: ['./button.component.scss'],
  // provide services here, so that each instance of component has its own service
  providers:  [ ButtonService ],
})
export class ButtonComponent {
  @Input('label') label: string;
  @Input('type') type: ButtonType = ButtonType.NORMAL;
  @Input('disabled') disabled: boolean = false;

  @Output('customEvent') customEvent: EventEmitter<any> = new EventEmitter<any>();
  
  constructor(public buttonService: ButtonService) { }

  onClick() {
    this.buttonService.increaseCounter();
    this.customEvent.emit('button clicked');
  }

}
