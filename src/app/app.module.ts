import { BrowserModule } from '@angular/platform-browser';
import {NgModule, CUSTOM_ELEMENTS_SCHEMA, Injector} from '@angular/core';
import { createCustomElement } from '@angular/elements';
import { AppComponent } from './app.component';
import { ButtonComponent } from './button/button.component';
import { InputComponent } from './input/input.component';
import {ButtonService} from './button/button.service';
import { ModalComponent } from './modal/modal.component';
import { StepperComponent } from './stepper/stepper.component';
import { WizardComponent } from './wizard/wizard.component';

@NgModule({
  declarations: [
    AppComponent,
    ButtonComponent,
    InputComponent,
    ModalComponent,
    StepperComponent,
    WizardComponent
  ],
  imports: [
    BrowserModule
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  providers: [
    ButtonService,
  ],
  // bootstrap: [AppComponent]
})
export class AppModule { 
  private webComponents: any[] = [
    {
      component: ButtonComponent,
      tag: 'vzlb-button',
    },
    {
      component: InputComponent,
      tag: 'vzlb-input',
    },
    {
      component: ModalComponent,
      tag: 'vzlb-modal',
    },
    {
      component: StepperComponent,
      tag: 'vzlb-stepper',
    },
    {
      component: WizardComponent,
      tag: 'vzlb-wizard',
    }
  ];
  
  constructor(private injector: Injector) {}

  ngDoBootstrap() {
    console.log('app bootstrapped')
    
    this.webComponents.forEach(
      webComponent => {
        const el = createCustomElement(webComponent.component, { injector: this.injector });
        customElements.define(webComponent.tag, el);
      }
    )
  }
}
