import {Component, ElementRef, HostBinding, OnInit, ViewEncapsulation} from '@angular/core';

@Component({
  selector: 'vzlb-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.scss'],
  // needed to make slots work in Angular
  encapsulation: ViewEncapsulation.ShadowDom,
})
export class ModalComponent implements OnInit {

  @HostBinding('style.position') private position = 'fixed';
  @HostBinding('style.z-index') private zIndex = '1000000';

  constructor(private host: ElementRef<HTMLElement>) { }

  ngOnInit(): void {
  }

  closeModal() {
    this.host.nativeElement.remove();
}

}
