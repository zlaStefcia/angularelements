import { Component, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'vzlb-wizard',
  templateUrl: './wizard.component.html',
  styleUrls: ['./wizard.component.scss'],
  encapsulation: ViewEncapsulation.ShadowDom,
})
export class WizardComponent implements OnInit {
  steps = ['kupcia', 'kupencja', 'kupsko'];
  constructor() { }

  ngOnInit(): void {}

}
