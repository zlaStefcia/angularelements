import { Component, Input } from '@angular/core';

@Component({
  selector: 'vzlb-stepper',
  templateUrl: './stepper.component.html',
  styleUrls: ['./stepper.component.scss']
})
export class StepperComponent {
  @Input('steps') steps: string[];

  constructor() { }

}
